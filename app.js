import path from 'node:path';
import assert from 'node:assert/strict';
import { createClient } from 'webdav';
import PQueue from 'p-queue';

const {
  WEBDAV_HOST,
  WEBDAV_USER,
  WEBDAV_PASSWORD,
  WEBDAV_DIRECTORY,
  FILES_REGEX,
  TARGET_NAME,
} = process.env;

const required = [WEBDAV_HOST, WEBDAV_USER, WEBDAV_PASSWORD, WEBDAV_DIRECTORY];
required.forEach((r) =>
  assert(r, 'Not all required environment variables were passed.')
);

const client = createClient(WEBDAV_HOST, {
  username: WEBDAV_USER,
  password: WEBDAV_PASSWORD,
});

let directoryItems = await client.getDirectoryContents(WEBDAV_DIRECTORY);

if (FILES_REGEX) {
  const regex = new RegExp(FILES_REGEX);
  directoryItems = directoryItems.filter((f) => regex.test(f.basename));
}

directoryItems = directoryItems
  .map((f) => ({
    ...f,
    lastmod: new Date(f.lastmod),
  }))
  .sort((a, b) => b.lastmod - a.lastmod);

if (directoryItems.length <= 1) {
  console.log('There is at most only one file present, nothing to do.');
  process.exit(0);
}

const latest = directoryItems.shift();
console.log(`Keeping "${latest.basename}`);

if (TARGET_NAME && latest.basename !== TARGET_NAME) {
  const target = path.join(latest.filename, '../', TARGET_NAME);
  await client.moveFile(latest.filename, target);
  console.log(`Moved latest to "${TARGET_NAME}"`);
}

const queue = new PQueue({ concurrency: 4 });

for (const file of directoryItems) {
  queue.add(() =>
    client
      .deleteFile(file.filename)
      .then(() => console.log(`Deleted "${file.basename}"`))
  );
}

await queue.onIdle();

console.log('Done.');
