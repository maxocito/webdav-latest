FROM node:18-alpine

ENV NODE_ENV=production
WORKDIR /app

COPY ["package.json", "pnpm-lock.yaml", "./"]
RUN npm install -g pnpm
RUN pnpm install

COPY . .
CMD ["node", "app.js"]